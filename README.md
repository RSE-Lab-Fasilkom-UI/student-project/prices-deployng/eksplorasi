# Eksplorasi DeployNG

## Kondisi Saat Ini

- [Skripsi Ichlasul Affan](https://gitlab.com/RSE-Lab-Fasilkom-UI/student-research/skripsi/1606895606-skripsi)
  menjelaskan proses _deployment_ yang sudah dipakai di _production_ SPLELive.
  Dokumen skripsi mengandung potongan-potongan kode Python untuk otomasi proses
  _deployment_.
- [Deployment CLI](https://gitlab.com/RSE-Lab-Fasilkom-UI/prices-2/abs-ifml-deployment-cli/-/tree/post-launch)
  adalah _tools_ komplit untuk membuat dan _deploy_ produk.
- [Skripsi Claudio Yosafat](https://gitlab.com/RSE-Lab-Fasilkom-UI/student-research/skripsi/implementasi-docker-deployment-pada-untuk-sple)
  meninjau pembuatan proses _deployment_ menggunakan teknologi _container_.
  Saat ini _production_ belum menggunakan proses berbasikan _container_.

## Belajar Deployment Automation Tools

- [Ansible 101 by Jeff Geerling](https://www.jeffgeerling.com/blog/2020/ansible-101-jeff-geerling-youtube-streaming-series)
  Serial video pembelajaran Ansible yang dibuat oleh salah satu kontributor
  terkemuka di komunitas Ansible.

## Lisensi

Ciptaan berupa teks catatan dan dokumentasi dapat diubah dan disebarluaskan
sesuai dengan ketentuan yang tercantum pada lisensi [Creative Commons Attribution 4.0 International](./LICENSE).
